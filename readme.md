## File Share with Chat Example##
This was create at the request of a user as an example to help them get started.  

It uses IdFTPServer and IdFTPClient to handle the file sharing piece and IdTCPServer and IdTCPClient for the chat.  

This is not a completed work but we would be happy to accept pull requests if anyone would like to clean things up. This is the result of approximately 4 hours of programming.  

# Things to Consider#
The error handling needs work  
Many of the FTP server function need to be finished out  
Reverse connection and STUN support would need to be added to make it viable (firewall issues)  
A user management system needs to be added to the server side  
A prompt to allow access needs to be added  
If someone was ambitious they could set up a guest system so the client is sandboxed until authorized  
Optional, allowing reverse file sharing where the host can see the client's files  
Optional, command handlers could be added to the chat stream so the host could forcible refresh the client's FTP window  
Optional, command handlers could be added to the chat stream so the host or client could request a file and a dialog would pop up on the receiver's end.

# Copyright and License#
All code is property of Delphi Mystics and Rapid Kinetics LLC. Delphi Mystics or Rapid Kinetics LLC reserves the right to revoke usage of this code at any time.  
Copyright (c) 2020 Rapid Kinetics LLC    