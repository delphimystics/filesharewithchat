unit dmMain;

interface uses
  {$region 'Includes'}
  System.SysUtils,
  System.Classes,
  System.Threading,
  My.Common,
  IdIOHandler,
  IdIOHandlerSocket,
  IdIOHandlerStack,
  IdZLibCompressorBase,
  IdCompressorZLib,
  IdExplicitTLSClientServerBase,
  IdFTP,
  IdBaseComponent,
  IdComponent,
  IdTCPConnection,
  IdTCPClient;
  {$endregion}

type
  TdatamodMain = class(TDataModule)
    ChatClient: TIdTCPClient;
    FTPClient: TIdFTP;
    FTPCompressor: TIdCompressorZLib;
    FTPStack: TIdIOHandlerStack;
    ChatStack: TIdIOHandlerStack;
    procedure ChatClientWork(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCount: Int64);
    procedure ChatClientWorkBegin(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCountMax: Int64);
    procedure ChatClientWorkEnd(ASender: TObject; AWorkMode: TWorkMode);
  private
    { Private declarations }
  public
    { Public declarations }
    OnChatUserMessage: TChatUserEvent;
    OnFTPLog: TFTPEvent;
    ChatPoller: ITask;
    ChatRun: Boolean;
    function Connect(const AHost: String; const AUser: String; const APass: String): TConnectionResponseRecord;
    procedure Disconnect;
    function SendChatMessage(const AMessage: String): String;
  end;

var
  datamodMain: TdatamodMain;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TdatamodMain }

function TdatamodMain.SendChatMessage(const AMessage: String): String;
begin
  if ChatClient.Connected = false then ChatClient.Connect;
  if ChatClient.Connected then
    begin
      ChatClient.Socket.WriteLn(AMessage);
      Result := AMessage;
    end
  else
    begin
      Result := ' { No Peer Connected} ';
    end;
end;

procedure TdatamodMain.ChatClientWork(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCount: Int64);
//var
//  sHost: String;
//  sMessage: String;
begin
//  sMessage := ChatClient.Socket.ReadLn;
//  sHost := ChatClient.Socket.Binding.PeerIP;
//  if sMessage = '' then Exit;
//  if Assigned(OnChatUserMessage) then
//    begin
//      OnChatUserMessage(Self,sHost,sMessage);
//    end;
end;

procedure TdatamodMain.ChatClientWorkBegin(ASender: TObject;
  AWorkMode: TWorkMode; AWorkCountMax: Int64);
begin
     //
end;

procedure TdatamodMain.ChatClientWorkEnd(ASender: TObject;
  AWorkMode: TWorkMode);
begin
//
end;

function TdatamodMain.Connect(const AHost, AUser, APass: String): TConnectionResponseRecord;
var
  sTmp: String;
begin
  if FTPClient.Connected then FTPClient.Disconnect;
  if ChatClient.Connected then ChatClient.Disconnect;
  ChatClient.Host := AHost;
  ChatClient.Port := 23871;
  ChatClient.Connect;
  Result.ChatConnected := ChatClient.Connected;

  ChatPoller := TTask.Create(
  procedure()
  var
    sHost: String;
    sMessage: String;
  begin
    ChatRun := True;
    while ChatRun do
      begin
        sMessage := ChatClient.Socket.ReadLn;
        sMessage := sMessage.Replace('SVR-HELLO-11654486168161561!','');

        sHost := ChatClient.Socket.Binding.PeerIP;
        if sMessage <> '' then
          begin
            if Assigned(OnChatUserMessage) then
              begin
                TThread.Synchronize ( nil, procedure()
                  begin
                    OnChatUserMessage(Self,sHost,sMessage);
                  end);
              end;
          end;
      end;
  end);
  ChatPoller.Start;

  FTPClient.Host := AHost;
  FTPClient.Port := 21;
  FTPClient.Username := AUser;
  FTPClient.Password := APass;
  FTPClient.Connect;
  Result.FTPConnected := FTPClient.Connected;

end;

procedure TdatamodMain.Disconnect;
begin
  if FTPClient.Connected then FTPClient.Disconnect;
  if ChatClient.Connected then ChatClient.Disconnect;
end;

end.
