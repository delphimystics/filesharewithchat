object datamodMain: TdatamodMain
  OldCreateOrder = False
  Height = 237
  Width = 482
  object ChatClient: TIdTCPClient
    IOHandler = ChatStack
    OnWork = ChatClientWork
    OnWorkBegin = ChatClientWorkBegin
    OnWorkEnd = ChatClientWorkEnd
    ConnectTimeout = 0
    IPVersion = Id_IPv4
    Port = 0
    ReadTimeout = -1
    Left = 264
    Top = 48
  end
  object FTPClient: TIdFTP
    IOHandler = FTPStack
    IPVersion = Id_IPv4
    Compressor = FTPCompressor
    ConnectTimeout = 0
    NATKeepAlive.UseKeepAlive = False
    NATKeepAlive.IdleTimeMS = 0
    NATKeepAlive.IntervalMS = 0
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    Left = 104
    Top = 40
  end
  object FTPCompressor: TIdCompressorZLib
    Left = 104
    Top = 104
  end
  object FTPStack: TIdIOHandlerStack
    Destination = ':21'
    MaxLineAction = maException
    Port = 21
    DefaultPort = 0
    ReadTimeout = 60000
    Left = 88
    Top = 168
  end
  object ChatStack: TIdIOHandlerStack
    MaxLineAction = maException
    Port = 0
    DefaultPort = 0
    Left = 224
    Top = 104
  end
end
