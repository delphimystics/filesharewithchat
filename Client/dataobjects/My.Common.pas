unit My.Common;

interface uses
  {$region 'Includes'}
  System.Classes,
  System.SysUtils,
  System.IOUtils,
  System.Threading,
  System.Diagnostics;
  {$endregion}

type
  TChatUserEvent = procedure(Sender: TObject; AUser: String; AData: String) of object;
  TFTPEvent = procedure(Sender: TObject; ALogData: String) of object;
  TConnectionResponseRecord = record
    ChatConnected: Boolean;
    FTPConnected: Boolean;
    ErrorMessage: String;
  end;
implementation

end.
