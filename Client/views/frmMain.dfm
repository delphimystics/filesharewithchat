object formMain: TformMain
  Left = 0
  Top = 0
  Caption = 'FSAC Client'
  ClientHeight = 529
  ClientWidth = 988
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 281
    Top = 25
    Height = 418
    ExplicitLeft = 384
    ExplicitTop = 120
    ExplicitHeight = 100
  end
  object Splitter2: TSplitter
    Left = 0
    Top = 443
    Width = 988
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    ExplicitLeft = 188
    ExplicitTop = 41
    ExplicitWidth = 217
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 988
    Height = 25
    Align = alTop
    TabOrder = 0
    object lblHost: TLabel
      Left = 1
      Top = 1
      Width = 73
      Height = 23
      Align = alLeft
      Caption = '     Host '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblPassword: TLabel
      Left = 389
      Top = 1
      Width = 72
      Height = 23
      Align = alLeft
      Caption = '     Pass '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ExplicitLeft = 377
    end
    object lblUser: TLabel
      Left = 195
      Top = 1
      Width = 73
      Height = 23
      Align = alLeft
      Caption = '     User '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ExplicitLeft = 189
    end
    object edtPassword: TEdit
      Left = 461
      Top = 1
      Width = 121
      Height = 23
      Align = alLeft
      PasswordChar = '*'
      TabOrder = 0
      Text = 'password123'
      ExplicitHeight = 21
    end
    object btnConnect: TButton
      Left = 912
      Top = 1
      Width = 75
      Height = 23
      Align = alRight
      Caption = 'Connect'
      TabOrder = 1
      OnClick = btnConnectClick
      ExplicitLeft = 488
      ExplicitTop = 16
      ExplicitHeight = 25
    end
    object edtHost: TEdit
      Left = 74
      Top = 1
      Width = 121
      Height = 23
      Align = alLeft
      TabOrder = 2
      Text = '127.0.0.1'
      ExplicitLeft = 120
      ExplicitTop = 6
      ExplicitHeight = 21
    end
    object edtUser: TEdit
      Left = 268
      Top = 1
      Width = 121
      Height = 23
      Align = alLeft
      TabOrder = 3
      Text = 'admin'
      ExplicitLeft = 120
      ExplicitTop = 6
      ExplicitHeight = 21
    end
  end
  object pnlLog: TPanel
    Left = 0
    Top = 446
    Width = 988
    Height = 83
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 216
    ExplicitWidth = 635
    object lbLog: TListBox
      Left = 1
      Top = 1
      Width = 986
      Height = 81
      Align = alClient
      ItemHeight = 13
      TabOrder = 0
      ExplicitLeft = 64
      ExplicitTop = 24
      ExplicitWidth = 121
      ExplicitHeight = 97
    end
  end
  object pnlChat: TPanel
    Left = 0
    Top = 25
    Width = 281
    Height = 418
    Align = alLeft
    TabOrder = 2
    ExplicitTop = 41
    ExplicitHeight = 172
    object lbChat: TListBox
      Left = 1
      Top = 1
      Width = 279
      Height = 390
      Align = alClient
      Enabled = False
      ItemHeight = 13
      TabOrder = 0
      ExplicitLeft = 32
      ExplicitTop = 16
      ExplicitWidth = 121
      ExplicitHeight = 97
    end
    object pnlSendBox: TPanel
      Left = 1
      Top = 391
      Width = 279
      Height = 26
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 288
      ExplicitWidth = 560
      object lblSend: TLabel
        Left = 1
        Top = 1
        Width = 52
        Height = 24
        Align = alLeft
        Caption = 'You> '
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitHeight = 23
      end
      object edtSend: TEdit
        Left = 53
        Top = 1
        Width = 150
        Height = 24
        Align = alClient
        Enabled = False
        TabOrder = 0
        OnKeyUp = edtSendKeyUp
        ExplicitWidth = 54
        ExplicitHeight = 21
      end
      object btnSend: TButton
        Left = 203
        Top = 1
        Width = 75
        Height = 24
        Align = alRight
        Caption = 'Send'
        Enabled = False
        TabOrder = 1
        OnClick = btnSendClick
        ExplicitLeft = 107
      end
    end
  end
  object pnlFiles: TPanel
    Left = 284
    Top = 25
    Width = 704
    Height = 418
    Align = alClient
    TabOrder = 3
    ExplicitLeft = 182
    ExplicitTop = 35
    ExplicitWidth = 447
    ExplicitHeight = 214
    object lbFiles: TListBox
      Left = 1
      Top = 1
      Width = 702
      Height = 416
      Align = alClient
      Enabled = False
      ItemHeight = 13
      TabOrder = 0
      ExplicitLeft = 24
      ExplicitTop = 24
      ExplicitWidth = 121
      ExplicitHeight = 97
    end
  end
end
