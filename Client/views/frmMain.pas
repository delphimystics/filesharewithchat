unit frmMain;

interface uses
  {$region 'Includes'}
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  My.Common,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  Vcl.ExtCtrls,
  IdFTPList;
  {$endregion}

type
  TformMain = class(TForm)
    pnlTop: TPanel;
    pnlLog: TPanel;
    pnlChat: TPanel;
    Splitter1: TSplitter;
    pnlFiles: TPanel;
    Splitter2: TSplitter;
    lbLog: TListBox;
    lbChat: TListBox;
    pnlSendBox: TPanel;
    lblSend: TLabel;
    edtSend: TEdit;
    btnSend: TButton;
    lbFiles: TListBox;
    edtPassword: TEdit;
    btnConnect: TButton;
    lblHost: TLabel;
    edtHost: TEdit;
    lblPassword: TLabel;
    lblUser: TLabel;
    edtUser: TEdit;
    procedure btnSendClick(Sender: TObject);
    procedure edtSendKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnConnectClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure OnChatMessage(Sender: TObject; AUser: String; AData: String);
    procedure OnFTPLog(Sender: TObject; ALogData: String);
  end;

var
  formMain: TformMain;

implementation

{$R *.dfm}

uses
  dmMain;


procedure TformMain.btnConnectClick(Sender: TObject);
var
  ConnectionResult: TConnectionResponseRecord;
  MyDirectory: TIdFTPListItems;
  iMyItem: Integer;
begin
  if btnConnect.Caption = 'Connect' then
    begin
      btnConnect.Caption := 'Disconnect';
      edtHost.Enabled := False;
      edtUser.Enabled := False;
      edtPassword.Enabled := False;
      ConnectionResult := datamodMain.Connect(edtHost.Text,edtUser.Text,edtPassword.Text);
      if ConnectionResult.ChatConnected then
        begin
          lbChat.Enabled := True;
          lblSend.Enabled := True;
          edtSend.Enabled := True;
          btnSend.Enabled := True;
          lbLog.Items.Add('Chat connected.');
        end
      else
        begin
          lbChat.Enabled := False;
          lblSend.Enabled := False;
          edtSend.Enabled := False;
          btnSend.Enabled := False;
          lbLog.Items.Add('CHAT NOT CONNECTED');
        end;
      if ConnectionResult.FTPConnected then
        begin
          lbFiles.Enabled := True;
          lbLog.Items.Add('FTP connected.');
          datamodMain.FTPClient.List;
          MyDirectory := datamodMain.FTPClient.DirectoryListing;
          lbFiles.Items.Clear;
          lbFiles.Items.Add('.');
          lbFiles.Items.Add('..');
          for iMyItem := 0 to MyDirectory.Count-1 do
            begin
              lbFiles.Items.Add(MyDirectory.Items[iMyItem].FileName);
            end;
        end
      else
        begin
          lbFiles.Enabled := False;
          lbLog.Items.Add('FTP NOT CONNECTED');
        end;
      if ConnectionResult.ErrorMessage <> '' then lbLog.Items.Add(ConnectionResult.ErrorMessage);
    end
  else
    begin
      btnConnect.Caption := 'Connect';
      edtHost.Enabled := True;
      edtUser.Enabled := True;
      edtPassword.Enabled := True;
      datamodMain.Disconnect;
      lbChat.Enabled := False;
      lblSend.Enabled := False;
      edtSend.Enabled := False;
      btnSend.Enabled := False;
      lbFiles.Enabled := False;
    end;
end;

procedure TformMain.btnSendClick(Sender: TObject);
var
  thisMessage: String;
begin
  btnSend.Enabled := False;
  edtSend.Enabled := False;
  thisMessage := edtSend.Text;
  edtSend.Text := '';
  btnSend.Enabled := True;
  edtSend.Enabled := True;
  edtSend.SetFocus;
  lbChat.Items.Add('(YOU) @ ' + FormatDateTime('hh:nn:ss',Now)+ '> ' + datamodMain.SendChatMessage(thisMessage));
end;

procedure TformMain.edtSendKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    btnSend.Click;
end;

procedure TformMain.FormCreate(Sender: TObject);
begin
  datamodMain.OnChatUserMessage := OnChatMessage;
  datamodMain.OnFTPLog := OnFTPLog;
end;

procedure TformMain.OnChatMessage(Sender: TObject; AUser, AData: String);
begin
  lbChat.Items.Add(AUser + ' @ ' + FormatDateTime('hh:nn:ss',Now)+ '> ' + AData);
end;

procedure TformMain.OnFTPLog(Sender: TObject; ALogData: String);
begin
  lbLog.Items.Add('[' + FormatDateTime('hh:nn:sss.zzz',Now) + '] '+ ALogData);
end;

end.
