object datamodMain: TdatamodMain
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 369
  Width = 589
  object FTPServer: TIdFTPServer
    Bindings = <>
    IOHandler = FTPStack
    OnConnect = FTPServerConnect
    OnDisconnect = FTPServerDisconnect
    Scheduler = FTPPool
    CommandHandlers = <>
    ExceptionReply.Code = '500'
    ExceptionReply.Text.Strings = (
      'Unknown Internal Error')
    Greeting.Code = '220'
    Greeting.Text.Strings = (
      'Indy FTP Server ready.')
    MaxConnectionReply.Code = '300'
    MaxConnectionReply.Text.Strings = (
      'Too many connections. Try again later.')
    ReplyTexts = <>
    ReplyUnknownCommand.Code = '500'
    ReplyUnknownCommand.Text.Strings = (
      'Unknown Command')
    Compressor = FTPCompressor
    AnonymousAccounts.Strings = (
      'anonymous'
      'ftp'
      'guest')
    UserAccounts = UserManager
    OnAfterUserLogin = FTPServerAfterUserLogin
    OnChangeDirectory = FTPServerChangeDirectory
    OnGetFileSize = FTPServerGetFileSize
    OnUserLogin = FTPServerUserLogin
    OnListDirectory = FTPServerListDirectory
    OnRenameFile = FTPServerRenameFile
    OnDeleteFile = FTPServerDeleteFile
    OnRetrieveFile = FTPServerRetrieveFile
    OnMakeDirectory = FTPServerMakeDirectory
    OnRemoveDirectory = FTPServerRemoveDirectory
    OnCombineFiles = FTPServerCombineFiles
    OnFileExistCheck = FTPServerFileExistCheck
    OnCompleteDirSize = FTPServerCompleteDirSize
    SITECommands = <>
    MLSDFacts = []
    ReplyUnknownSITCommand.Code = '500'
    ReplyUnknownSITCommand.Text.Strings = (
      'Invalid SITE command.')
    Left = 280
    Top = 168
  end
  object FTPCompressor: TIdCompressorZLib
    Left = 248
    Top = 64
  end
  object FTPStack: TIdServerIOHandlerStack
    Left = 280
    Top = 272
  end
  object FTPPool: TIdSchedulerOfThreadPool
    MaxThreads = 0
    Left = 200
    Top = 240
  end
  object UserManager: TIdUserManager
    Accounts = <
      item
        UserName = 'admin'
        Password = 'password123'
        RealName = 'Administrator'
      end>
    Options = []
    Left = 104
    Top = 104
  end
  object ChatServer: TIdTCPServer
    Bindings = <>
    DefaultPort = 23871
    IOHandler = ChatStack
    OnConnect = ChatServerConnect
    OnDisconnect = ChatServerDisconnect
    Scheduler = ChatPool
    OnExecute = ChatServerExecute
    Left = 464
    Top = 96
  end
  object ChatPool: TIdSchedulerOfThreadPool
    MaxThreads = 0
    Left = 432
    Top = 176
  end
  object ChatStack: TIdServerIOHandlerStack
    Left = 408
    Top = 40
  end
end
