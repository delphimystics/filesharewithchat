unit dmMain;

interface uses
  {$region 'Includes'}
  System.SysUtils,
  System.Classes,
  IdBaseComponent,
  IdComponent,
  IdCustomTCPServer,
  IdTCPServer,
  IdCmdTCPServer,
  IdExplicitTLSClientServerBase,
  IdFTPServer,
  IdUserAccounts,
  IdScheduler,
  IdSchedulerOfThread,
  IdSchedulerOfThreadPool,
  IdServerIOHandler,
  IdServerIOHandlerSocket,
  IdServerIOHandlerStack,
  IdZLibCompressorBase,
  IdFTPBaseFileSystem,
  My.FTPFileSystem,
  My.Common,
  IdCompressorZLib,
  IdContext,
  IdFTPList,
  IdFTPListOutput;
  {$endregion}

type
  TdatamodMain = class(TDataModule)
    FTPServer: TIdFTPServer;
    FTPCompressor: TIdCompressorZLib;
    FTPStack: TIdServerIOHandlerStack;
    FTPPool: TIdSchedulerOfThreadPool;
    UserManager: TIdUserManager;
    ChatServer: TIdTCPServer;
    ChatPool: TIdSchedulerOfThreadPool;
    ChatStack: TIdServerIOHandlerStack;
    procedure DataModuleCreate(Sender: TObject);
    procedure FTPServerUserAccount(ASender: TIdFTPServerContext; const AUsername, APassword, AAcount: string; var AAuthenticated: Boolean);
    procedure FTPServerUserLogin(ASender: TIdFTPServerContext; const AUsername, APassword: string; var AAuthenticated: Boolean);
    procedure ChatServerConnect(AContext: TIdContext);
    procedure ChatServerDisconnect(AContext: TIdContext);
    procedure ChatServerExecute(AContext: TIdContext);
    procedure FTPServerAfterUserLogin(ASender: TIdFTPServerContext);
    procedure FTPServerChangeDirectory(ASender: TIdFTPServerContext;
      var VDirectory: string);
    procedure FTPServerCombineFiles(ASender: TIdFTPServerContext;
      const ATargetFileName: string; AParts: TStrings);
    procedure FTPServerCompleteDirSize(ASender: TIdFTPServerContext;
      const APathName: string; var VIsAFile: Boolean; var VSpace: Int64);
    procedure FTPServerConnect(AContext: TIdContext);
    procedure FTPServerDeleteFile(ASender: TIdFTPServerContext;
      const APathName: string);
    procedure FTPServerDisconnect(AContext: TIdContext);
    procedure FTPServerFileExistCheck(ASender: TIdFTPServerContext;
      const APathName: string; var VExist: Boolean);
    procedure FTPServerGetFileSize(ASender: TIdFTPServerContext;
      const AFilename: string; var VFileSize: Int64);
    procedure FTPServerMakeDirectory(ASender: TIdFTPServerContext;
      var VDirectory: string);
    procedure FTPServerRenameFile(ASender: TIdFTPServerContext;
      const ARenameFromFile, ARenameToFile: string);
    procedure FTPServerRetrieveFile(ASender: TIdFTPServerContext;
      const AFileName: string; var VStream: TStream);
    procedure FTPServerRemoveDirectory(ASender: TIdFTPServerContext;
      var VDirectory: string);
    procedure FTPServerListDirectory(ASender: TIdFTPServerContext;
      const APath: string; ADirectoryListing: TIdFTPListOutput; const ACmd,
      ASwitches: string);
  private
    { Private declarations }
    IdFS: TMyFTPFileSystem;
  public
    { Public declarations }
    OnChatUserStatus: TChatUserEvent;
    OnChatUserMessage: TChatUserEvent;
    CurrentPeer: TIdContext;
    OnFTPLog: TFTPEvent;
    FBaseDir: String;
    FCurrentDir: String;
    function SendChatMessage(const AMessage: String): String;
  end;

var
  datamodMain: TdatamodMain;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure TdatamodMain.ChatServerConnect(AContext: TIdContext);
var
  sHost: String;
begin
  sHost := AContext.Connection.Socket.Binding.PeerIP;
  CurrentPeer := AContext;
  CurrentPeer.Connection.Socket.Write('SVR-HELLO-11654486168161561!');
  if Assigned(OnChatUserStatus) then
    begin
      OnChatUserStatus(Self,sHost,'Join');
    end;
end;

procedure TdatamodMain.ChatServerDisconnect(AContext: TIdContext);
var
  sHost: String;
begin
  sHost := AContext.Connection.Socket.Binding.PeerIP;
  if Assigned(OnChatUserStatus) then
    begin
      OnChatUserStatus(Self,sHost,'Leave');
    end;
end;

procedure TdatamodMain.ChatServerExecute(AContext: TIdContext);
var
  sMessage: String;
  sHost: String;
begin
  sMessage := AContext.Connection.Socket.ReadLn();
  sHost := AContext.Connection.Socket.Binding.PeerIP;
  if sMessage = '' then Exit;
  if Assigned(OnChatUserMessage) then
    begin
      OnChatUserMessage(Self,sHost,sMessage);
    end;
end;

procedure TdatamodMain.DataModuleCreate(Sender: TObject);
begin
  IdFS := TMyFTPFileSystem.Create;
//  FTPServer.FTPFileSystem := IdFS;
  FBaseDir := 'c:\';
end;

procedure TdatamodMain.FTPServerAfterUserLogin(ASender: TIdFTPServerContext);
begin
  if Assigned(OnFTPLog) then
    OnFTPLog(Self,'User Logged In.');
end;

procedure TdatamodMain.FTPServerChangeDirectory(ASender: TIdFTPServerContext;
  var VDirectory: string);
begin
  if Assigned(OnFTPLog) then
    OnFTPLog(Self,'Change Directory: ' + VDirectory);
end;

procedure TdatamodMain.FTPServerCombineFiles(ASender: TIdFTPServerContext;
  const ATargetFileName: string; AParts: TStrings);
begin
  if Assigned(OnFTPLog) then
    OnFTPLog(Self,'Combine Files: ' + ATargetFileName + ' into ' + AParts.Text);
end;

procedure TdatamodMain.FTPServerCompleteDirSize(ASender: TIdFTPServerContext;
  const APathName: string; var VIsAFile: Boolean; var VSpace: Int64);
begin
  if Assigned(OnFTPLog) then
    OnFTPLog(Self,'Complete Dir Size: ' + APathName + ' Size: ' + VSpace.ToString);
end;

procedure TdatamodMain.FTPServerConnect(AContext: TIdContext);
begin
  if Assigned(OnFTPLog) then
    OnFTPLog(Self, AContext.Connection.Socket.Binding.PeerIP + ' connected.');
end;

procedure TdatamodMain.FTPServerDeleteFile(ASender: TIdFTPServerContext;
  const APathName: string);
begin
  if Assigned(OnFTPLog) then
    OnFTPLog(Self, 'Delete file: ' + APathName);
end;

procedure TdatamodMain.FTPServerDisconnect(AContext: TIdContext);
begin
  if Assigned(OnFTPLog) then
    OnFTPLog(Self, AContext.Connection.Socket.Binding.PeerIP + ' disconnected.');
end;

procedure TdatamodMain.FTPServerFileExistCheck(ASender: TIdFTPServerContext;
  const APathName: string; var VExist: Boolean);
begin
  if Assigned(OnFTPLog) then
    begin
      if FileExists(APathName) then
        OnFTPLog(Self, 'File Exists: ' + APathName)
      else
        OnFTPLog(Self, 'File Does Not Exist: ' + APathName);
    end;
end;

procedure TdatamodMain.FTPServerGetFileSize(ASender: TIdFTPServerContext;
  const AFilename: string; var VFileSize: Int64);
begin
  if Assigned(OnFTPLog) then
    OnFTPLog(Self, 'File ' + AFilename + ' Size: ' + VFileSize.ToString);
end;

procedure TdatamodMain.FTPServerListDirectory(ASender: TIdFTPServerContext;
  const APath: string; ADirectoryListing: TIdFTPListOutput; const ACmd,
  ASwitches: string);
Var
  Path    : String;
  SR      : TSearchRec;
  DirList : TStrings;
  thisItem: TIdFTPListOutputItem;
  iCheck: Integer;
begin
  Path := FBaseDir + APath.Replace('/','\');
  try
    if FindFirst(Path + '*.*', faDirectory, SR) = 0 then
      begin
        repeat
          thisItem := ADirectoryListing.Add;
          //TODO: Clean up returned data
          thisItem.LastAccessDate := SR.TimeStamp;
          thisItem.LastAccessDateGMT := SR.TimeStamp;
          thisItem.CreationDate := SR.TimeStamp;
          thisItem.CreationDateGMT := SR.TimeStamp;
          thisItem.ModifiedDateGMT := SR.TimeStamp;
          thisItem.ModifiedDate := SR.TimeStamp;
          thisItem.WinAttribs := SR.Attr;
          thisItem.Size := SR.Size;
          thisItem.LocalFileName := SR.Name;
          thisItem.FileName := SR.Name;
          iCheck := SR.Attr;
          case SR.Attr of
            16: thisItem.ItemType := ditDirectory;
            32: thisItem.ItemType := ditFile;
          else
            thisItem.ItemType := ditFile;
          end;
        until FindNext(SR) <> 0;
        FindClose(SR);
      end;
  finally
    //
  end;
end;

procedure TdatamodMain.FTPServerMakeDirectory(ASender: TIdFTPServerContext;
  var VDirectory: string);
begin
  if Assigned(OnFTPLog) then
    OnFTPLog(Self, 'Make Directory: ' + VDirectory);
end;

procedure TdatamodMain.FTPServerRemoveDirectory(ASender: TIdFTPServerContext;
  var VDirectory: string);
begin
  if Assigned(OnFTPLog) then
    OnFTPLog(Self, 'Remove: ' + VDirectory);
end;

procedure TdatamodMain.FTPServerRenameFile(ASender: TIdFTPServerContext;
  const ARenameFromFile, ARenameToFile: string);
begin
  if Assigned(OnFTPLog) then
    OnFTPLog(Self, 'Rename: ' + ARenameFromFile + ' to ' + ARenameToFile);
end;

procedure TdatamodMain.FTPServerRetrieveFile(ASender: TIdFTPServerContext;
  const AFileName: string; var VStream: TStream);
begin
  if Assigned(OnFTPLog) then
    OnFTPLog(Self, 'Get: ' + AFileName);
end;

procedure TdatamodMain.FTPServerUserAccount(ASender: TIdFTPServerContext;
  const AUsername, APassword, AAcount: string; var AAuthenticated: Boolean);
var
  iThisAccount: Integer;
begin
  if UserManager.Accounts.Count = 0 then
    begin
      AAuthenticated := False;
      Exit;
    end;
  for iThisAccount := 0 to UserManager.Accounts.Count-1 do
    begin
      if ((AUsername = UserManager.Accounts.Items[iThisAccount].UserName) and (APassword = UserManager.Accounts.Items[iThisAccount].Password)) then
        begin
          AAuthenticated := True;
          Break;
        end;
    end;
end;

procedure TdatamodMain.FTPServerUserLogin(ASender: TIdFTPServerContext;
  const AUsername, APassword: string; var AAuthenticated: Boolean);
var
  iThisAccount: Integer;
begin
  if UserManager.Accounts.Count = 0 then
    begin
      AAuthenticated := False;
      Exit;
    end;
  for iThisAccount := 0 to UserManager.Accounts.Count-1 do
    begin
      if ((AUsername = UserManager.Accounts.Items[iThisAccount].UserName) and (APassword = UserManager.Accounts.Items[iThisAccount].Password)) then
        begin
          AAuthenticated := True;
          Break;
        end;
    end;
end;

function TdatamodMain.SendChatMessage(const AMessage: String): String;
begin
  if Assigned(CurrentPeer) then
    begin
      if CurrentPeer.Connection.Connected then
        begin
          CurrentPeer.Connection.Socket.WriteLn(AMessage);
          Result := AMessage;
        end
      else
        begin
          Result := ' {Peer No Longer Connected} ';
        end;
    end
  else
    begin
      Result := ' { No Peer Connected} ';
    end;
end;

end.
