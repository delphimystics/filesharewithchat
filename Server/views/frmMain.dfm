object formMain: TformMain
  Left = 0
  Top = 0
  Caption = 'FSAC Server'
  ClientHeight = 437
  ClientWidth = 686
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object splitUsersVertical: TSplitter
    Left = 121
    Top = 22
    Height = 415
    ExplicitLeft = 352
    ExplicitTop = 120
    ExplicitHeight = 100
  end
  object lbUsers: TListBox
    Left = 0
    Top = 22
    Width = 121
    Height = 415
    Align = alLeft
    ItemHeight = 13
    TabOrder = 0
    ExplicitLeft = 288
    ExplicitTop = 120
    ExplicitHeight = 97
  end
  object pnlBody: TPanel
    Left = 124
    Top = 22
    Width = 562
    Height = 415
    Align = alClient
    TabOrder = 1
    ExplicitLeft = 256
    ExplicitTop = 216
    ExplicitWidth = 185
    ExplicitHeight = 221
    object splitBodyHorizontal: TSplitter
      Left = 1
      Top = 314
      Width = 560
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 1
      ExplicitWidth = 122
    end
    object lbFTPLog: TListBox
      Left = 1
      Top = 317
      Width = 560
      Height = 97
      Align = alBottom
      ItemHeight = 13
      TabOrder = 0
      ExplicitLeft = 160
      ExplicitTop = 304
      ExplicitWidth = 121
    end
    object lbChat: TListBox
      Left = 1
      Top = 1
      Width = 560
      Height = 287
      Align = alClient
      ItemHeight = 13
      TabOrder = 1
      ExplicitLeft = 288
      ExplicitTop = 192
      ExplicitWidth = 121
      ExplicitHeight = 97
    end
    object pnlSendBox: TPanel
      Left = 1
      Top = 288
      Width = 560
      Height = 26
      Align = alBottom
      TabOrder = 2
      object lblSend: TLabel
        Left = 1
        Top = 1
        Width = 52
        Height = 24
        Align = alLeft
        Caption = 'You> '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitHeight = 23
      end
      object edtSend: TEdit
        Left = 53
        Top = 1
        Width = 431
        Height = 24
        Align = alClient
        TabOrder = 0
        OnKeyUp = edtSendKeyUp
        ExplicitLeft = 47
        ExplicitTop = -1
      end
      object btnSend: TButton
        Left = 484
        Top = 1
        Width = 75
        Height = 24
        Align = alRight
        Caption = 'Send'
        TabOrder = 1
        OnClick = btnSendClick
        ExplicitLeft = 490
        ExplicitTop = -1
      end
    end
  end
  object tbMain: TToolBar
    Left = 0
    Top = 0
    Width = 686
    Height = 22
    ButtonHeight = 21
    ButtonWidth = 31
    Caption = 'Main'
    ShowCaptions = True
    TabOrder = 2
    object btnStart: TToolButton
      Left = 0
      Top = 0
      Caption = 'Start'
      ImageIndex = 0
      OnClick = btnStartClick
    end
  end
end
