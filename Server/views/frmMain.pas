unit frmMain;

interface uses
  {$region 'Includes'}
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  My.Common,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.ComCtrls,
  Vcl.ToolWin,
  Vcl.ExtCtrls,
  Vcl.StdCtrls;
  {$endregion}

type
  TformMain = class(TForm)
    lbUsers: TListBox;
    splitUsersVertical: TSplitter;
    pnlBody: TPanel;
    lbFTPLog: TListBox;
    lbChat: TListBox;
    splitBodyHorizontal: TSplitter;
    tbMain: TToolBar;
    btnStart: TToolButton;
    pnlSendBox: TPanel;
    lblSend: TLabel;
    edtSend: TEdit;
    btnSend: TButton;
    procedure btnStartClick(Sender: TObject);
    procedure btnSendClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edtSendKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure OnChatUserStatus(Sender: TObject; AUser: String; AData: String);
    procedure OnChatUserMessage(Sender: TObject; AUser: String; AData: String);
    procedure OnFTPLog(Sender: TObject; ALogData: String);

  end;

var
  formMain: TformMain;

implementation

{$R *.dfm}

uses
  dmMain;

procedure TformMain.btnSendClick(Sender: TObject);
var
  thisMessage: String;
begin
  btnSend.Enabled := False;
  edtSend.Enabled := False;
  thisMessage := edtSend.Text;
  edtSend.Text := '';
  btnSend.Enabled := True;
  edtSend.Enabled := True;
  edtSend.SetFocus;
  lbChat.Items.Add('(YOU) @ ' + FormatDateTime('hh:nn:ss',Now)+ '> ' + datamodMain.SendChatMessage(thisMessage));
end;

procedure TformMain.btnStartClick(Sender: TObject);
begin
  if btnStart.Caption = 'Start' then
    begin
      btnStart.Caption := 'Stop';
      datamodMain.FTPServer.Active := True;
      datamodMain.ChatServer.Active := True;
    end
  else
    begin
      btnStart.Caption := 'Start';
      datamodMain.FTPServer.Active := False;
      datamodMain.ChatServer.Active := False;
    end;
end;

procedure TformMain.edtSendKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    btnSend.Click;

end;

procedure TformMain.FormCreate(Sender: TObject);
begin
  datamodMain.OnChatUserStatus := OnChatUserStatus;
  datamodMain.OnChatUserMessage := OnChatUserMessage;
  datamodMain.OnFTPLog := OnFTPLog;
end;

procedure TformMain.OnChatUserMessage(Sender: TObject; AUser, AData: String);
begin
  lbChat.Items.Add(AUser + ' @ ' + FormatDateTime('hh:nn:ss',Now)+ '> ' + AData);
end;

procedure TformMain.OnChatUserStatus(Sender: TObject; AUser, AData: String);
begin
  if AData = 'Join' then
    begin
      if lbUsers.Items.IndexOf(AUser) < 0 then
        begin
          lbUsers.Items.Add(AUser);
          lbChat.Items.Add(AUser + ' @ ' + FormatDateTime('hh:nn:ss',Now)+ '> { JOINED }');
        end;
    end;
  if AData = 'Leave' then
    begin
      if lbUsers.Items.IndexOf(AUser) > -1 then
        begin
          lbUsers.Items.Delete(lbUsers.Items.IndexOf(AUser));
          lbChat.Items.Add(AUser + ' @ ' + FormatDateTime('hh:nn:ss',Now)+ '> { LEFT }');
        end;
    end;
end;

procedure TformMain.OnFTPLog(Sender: TObject; ALogData: String);
begin
  lbFTPLog.Items.Add('[' + FormatDateTime('hh:nn:sss.zzz',Now) + '] '+ ALogData);
end;

end.
