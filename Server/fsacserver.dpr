program fsacserver;

uses
  Vcl.Forms,
  frmMain in 'views\frmMain.pas' {formMain},
  dmMain in 'datamodules\dmMain.pas' {datamodMain: TDataModule},
  My.FTPFileSystem in 'dataobjects\My.FTPFileSystem.pas',
  My.Common in 'dataobjects\My.Common.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TdatamodMain, datamodMain);
  Application.CreateForm(TformMain, formMain);
  Application.Run;
end.
