unit My.FTPFileSystem;

interface uses
  {$region 'Include'}
  System.Classes,
  System.SysUtils,
  System.IOUtils,
  IdBaseComponent,
  IdException,
  IdFTPList,
  IdFTPListOutput,
  IdFTPServerContextBase,
  IdFTPBaseFileSystem;
  {$endregion}
type
  TMyFTPFileSystem = class(TIdFTPBaseFileSystem)
  private
    FCurrentDir: String;
    FBaseDir: String;
    procedure ChangeDir(AContext : TIdFTPServerContextBase; var VDirectory: TIdFTPFileName);
    procedure GetFileSize(AContext : TIdFTPServerContextBase; const AFilename: TIdFTPFileName; var VFileSize: Int64);
    procedure GetFileDate(AContext : TIdFTPServerContextBase; const AFilename: TIdFTPFileName; var VFileDate: TDateTime);
    procedure ListDirectory(AContext : TIdFTPServerContextBase; const APath: TIdFTPFileName; ADirectoryListing: TIdFTPListOutput; const ACmd, ASwitches : String);
    procedure RenameFile(AContext : TIdFTPServerContextBase; const ARenameToFile: TIdFTPFileName);
    procedure DeleteFile(AContext : TIdFTPServerContextBase; const APathName: TIdFTPFileName);
    procedure RetrieveFile(AContext : TIdFTPServerContextBase; const AFileName: TIdFTPFileName; var VStream: TStream);
    procedure StoreFile(AContext : TIdFTPServerContextBase; const AFileName: TIdFTPFileName; AAppend: Boolean; var VStream: TStream);
    procedure MakeDirectory(AContext : TIdFTPServerContextBase; var VDirectory: TIdFTPFileName);
    procedure RemoveDirectory(AContext : TIdFTPServerContextBase; var VDirectory: TIdFTPFileName);
    procedure SetModifiedFileDate(AContext : TIdFTPServerContextBase; const AFileName: TIdFTPFileName; var VDateTime: TDateTime);
    procedure GetCRCCalcStream(AContext : TIdFTPServerContextBase; const AFileName: TIdFTPFileName; var VStream : TStream);
    procedure CombineFiles(AContext : TIdFTPServerContextBase; const ATargetFileName: TIdFTPFileName; AParts: TStrings);
  public
    property BaseDir: String read FBaseDir write FBaseDir;
  end;

implementation

{ TMyFTPFileSystem }

procedure TMyFTPFileSystem.ChangeDir(AContext: TIdFTPServerContextBase; var VDirectory: TIdFTPFileName);
var
  slPath: TStrings;
  sThisString: String;
begin
  if VDirectory = '' then Exit;
  if VDirectory = '.' then Exit;
  if VDirectory = '\' then
    begin
      FCurrentDir := '\';
      exit;
    end;

  if ((VDirectory = '..') and (FCurrentDir <> '\')) then
    begin
      slPath := TStringList.Create;
      slPath.Delimiter := '\';
      slPath.DelimitedText := FCurrentDir;
      FCurrentDir := '\';
      for sThisString in slPath do
         begin
          if ((sThisString <> '') and (FCurrentDir <> '\')) then
            begin
              FCurrentDir := FCurrentDir + '\' + sThisString;
            end
          else if ((sThisString <> '') and (FCurrentDir = '\')) then
            begin
              FCurrentDir := FCurrentDir + sThisString;
            end;
         end;
      exit;
    end;
  FCurrentDir := FCurrentDir + '\' + VDirectory;
end;

procedure TMyFTPFileSystem.CombineFiles(AContext: TIdFTPServerContextBase; const ATargetFileName: TIdFTPFileName; AParts: TStrings);
begin
  //TODO: Add Combine Files
end;

procedure TMyFTPFileSystem.DeleteFile(AContext: TIdFTPServerContextBase; const APathName: TIdFTPFileName);
begin
  if FileExists(FBaseDir + APathName) then
    System.SysUtils.DeleteFile(APathName);
end;

procedure TMyFTPFileSystem.GetCRCCalcStream(AContext: TIdFTPServerContextBase; const AFileName: TIdFTPFileName; var VStream: TStream);
begin
  //TODO: Add GetCRCCalcStream
end;

procedure TMyFTPFileSystem.GetFileDate(AContext: TIdFTPServerContextBase; const AFilename: TIdFTPFileName; var VFileDate: TDateTime);
begin
  //TODO: Make this check if file exists and the date of the file.
  VFileDate := Now;
end;

procedure TMyFTPFileSystem.GetFileSize(AContext: TIdFTPServerContextBase; const AFilename: TIdFTPFileName; var VFileSize: Int64);
var
  MyFile: File;
begin
  AssignFile(MyFile,FBaseDir + AFileName);
  VFileSize := FileSize(MyFile);
  CloseFile(MyFile);
end;

procedure TMyFTPFileSystem.ListDirectory(AContext: TIdFTPServerContextBase; const APath: TIdFTPFileName; ADirectoryListing: TIdFTPListOutput; const ACmd,  ASwitches: String);
Var
  Path    : String;
  SR      : TSearchRec;
  DirList : TStrings;
  thisItem: TIdFTPListOutputItem;
begin
  Path := FBaseDir + APath;
  try
    if FindFirst(Path + '*.*', faArchive, SR) = 0 then
      begin
        repeat
          thisItem := ADirectoryListing.Add;
          //TODO: Clean up returned data
          thisItem.LastAccessDate := SR.TimeStamp;
          thisItem.LastAccessDateGMT := SR.TimeStamp;
          thisItem.CreationDate := SR.TimeStamp;
          thisItem.CreationDateGMT := SR.TimeStamp;
          thisItem.ModifiedDateGMT := SR.TimeStamp;
          thisItem.ModifiedDate := SR.TimeStamp;
          thisItem.WinAttribs := SR.Attr;
          thisItem.Size := SR.Size;
          thisItem.LocalFileName := SR.Name;
          thisItem.FileName := SR.Name;
        until FindNext(SR) <> 0;
        FindClose(SR);
      end;
  finally
    //
  end;
end;

procedure TMyFTPFileSystem.MakeDirectory(AContext: TIdFTPServerContextBase; var VDirectory: TIdFTPFileName);
begin
  ForceDirectories(BaseDir + VDirectory);
end;

procedure TMyFTPFileSystem.RemoveDirectory(AContext: TIdFTPServerContextBase; var VDirectory: TIdFTPFileName);
begin
  //TODO: This probably doesn't work.
  System.SysUtils.DeleteFile(BaseDir + VDirectory);
end;

procedure TMyFTPFileSystem.RenameFile(AContext: TIdFTPServerContextBase; const ARenameToFile: TIdFTPFileName);
begin
  //TODO: This doesn't work, for some reason they are missing a parameter for this function.
  System.SysUtils.RenameFile('MissingOldFileName',ARenameToFile);
end;

procedure TMyFTPFileSystem.RetrieveFile(AContext: TIdFTPServerContextBase; const AFileName: TIdFTPFileName; var VStream: TStream);
begin
  VStream := TFileStream.Create(BaseDir + AFileName,fmShareExclusive);
end;

procedure TMyFTPFileSystem.SetModifiedFileDate(AContext: TIdFTPServerContextBase; const AFileName: TIdFTPFileName; var VDateTime: TDateTime);
begin
  //TODO: Finish SetModifiedFileDate
end;

procedure TMyFTPFileSystem.StoreFile(AContext: TIdFTPServerContextBase; const AFileName: TIdFTPFileName; AAppend: Boolean; var VStream: TStream);
begin
  //TODO: Modify to accept append to file.
  VStream := TFileStream.Create(BaseDir + AFileName, fmShareExclusive);
end;

end.
